__author__ = "andy"

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import platform
import datetime


class ExceptionNotifier(object):
    """
    ...
    """

    def __init__(self, email_adr, password, send_mail):
        self._server = smtplib.SMTP('smtp.gmail.com', 587)
        self._uname = email_adr
        self._pass = password
        # self._server.starttls()
        # self._server.login(email_adr, password)
        self._email_adr = email_adr
        self._send_email_adr = send_mail

    def send_mail(self, body):
        msg = MIMEMultipart()
        msg['From'] = self._email_adr
        msg['To'] = self._send_email_adr
        msg['Subject'] = "error of script [{}] on machine [{}] on date [{}]".format(__file__.split("/")[-1],
                                                                                    platform.node(),
                                                                                    datetime.datetime.now())

        msg.attach(MIMEText(body, 'plain'))

        machine_info = "Machine name: {}\n".format(platform.node())
        architecture = "Machine: {}]\n".format(platform.machine())
        cpu_info = "cpu: {} \n".format(platform.processor())
        os_info = "os: {} \n".format(platform.platform())

        system_info = "\n System Info: \n" + machine_info + architecture + cpu_info + os_info
        msg.attach(MIMEText(system_info, 'plain'))

        self._server.starttls()
        self._server.login(self._uname, self._pass)
        text = msg.as_string()
        self._server.sendmail(self._email_adr, self._send_email_adr, text)
        self._server.quit()
